﻿namespace DFMapper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.generateButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.biomeButton = new System.Windows.Forms.Button();
            this.biomeLabel = new System.Windows.Forms.Label();
            this.rainfallButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.vegButton = new System.Windows.Forms.Button();
            this.aboutButton = new System.Windows.Forms.Button();
            this.vegCheckbox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.contrastCheckbox = new System.Windows.Forms.CheckBox();
            this.gradientCheckbox = new System.Windows.Forms.CheckBox();
            this.rainCheckbox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // generateButton
            // 
            this.generateButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.generateButton.Enabled = false;
            this.generateButton.Location = new System.Drawing.Point(12, 377);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(336, 23);
            this.generateButton.TabIndex = 0;
            this.generateButton.Text = "&Generate Map";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // biomeButton
            // 
            this.biomeButton.Location = new System.Drawing.Point(165, 30);
            this.biomeButton.Name = "biomeButton";
            this.biomeButton.Size = new System.Drawing.Size(75, 23);
            this.biomeButton.TabIndex = 1;
            this.biomeButton.Text = "Browse...";
            this.biomeButton.UseVisualStyleBackColor = true;
            this.biomeButton.Click += new System.EventHandler(this.biomeButton_Click);
            // 
            // biomeLabel
            // 
            this.biomeLabel.AutoSize = true;
            this.biomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biomeLabel.ForeColor = System.Drawing.Color.Red;
            this.biomeLabel.Location = new System.Drawing.Point(6, 35);
            this.biomeLabel.Name = "biomeLabel";
            this.biomeLabel.Size = new System.Drawing.Size(153, 13);
            this.biomeLabel.TabIndex = 2;
            this.biomeLabel.Text = "Standard biome+site map:";
            // 
            // rainfallButton
            // 
            this.rainfallButton.Location = new System.Drawing.Point(371, 41);
            this.rainfallButton.Name = "rainfallButton";
            this.rainfallButton.Size = new System.Drawing.Size(75, 23);
            this.rainfallButton.TabIndex = 3;
            this.rainfallButton.Text = "Browse...";
            this.rainfallButton.UseVisualStyleBackColor = true;
            this.rainfallButton.Click += new System.EventHandler(this.rainfallButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Current vegetation export:";
            // 
            // vegButton
            // 
            this.vegButton.Location = new System.Drawing.Point(371, 15);
            this.vegButton.Name = "vegButton";
            this.vegButton.Size = new System.Drawing.Size(75, 23);
            this.vegButton.TabIndex = 5;
            this.vegButton.Text = "Browse...";
            this.vegButton.UseVisualStyleBackColor = true;
            this.vegButton.Click += new System.EventHandler(this.vegButton_Click);
            // 
            // aboutButton
            // 
            this.aboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutButton.Location = new System.Drawing.Point(435, 377);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(29, 23);
            this.aboutButton.TabIndex = 8;
            this.aboutButton.Text = "?";
            this.aboutButton.UseVisualStyleBackColor = true;
            this.aboutButton.Click += new System.EventHandler(this.aboutButton_Click);
            // 
            // vegCheckbox
            // 
            this.vegCheckbox.AutoSize = true;
            this.vegCheckbox.Location = new System.Drawing.Point(6, 19);
            this.vegCheckbox.Name = "vegCheckbox";
            this.vegCheckbox.Size = new System.Drawing.Size(106, 17);
            this.vegCheckbox.TabIndex = 9;
            this.vegCheckbox.Text = "Vegetation Layer";
            this.vegCheckbox.UseVisualStyleBackColor = true;
            this.vegCheckbox.CheckedChanged += new System.EventHandler(this.vegCheckbox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.biomeLabel);
            this.groupBox1.Controls.Add(this.biomeButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(452, 70);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Required";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(354, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "&Settings...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.contrastCheckbox);
            this.groupBox2.Controls.Add(this.gradientCheckbox);
            this.groupBox2.Controls.Add(this.rainCheckbox);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.vegCheckbox);
            this.groupBox2.Controls.Add(this.vegButton);
            this.groupBox2.Controls.Add(this.rainfallButton);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(452, 283);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Optional";
            // 
            // contrastCheckbox
            // 
            this.contrastCheckbox.AutoSize = true;
            this.contrastCheckbox.Location = new System.Drawing.Point(6, 97);
            this.contrastCheckbox.Name = "contrastCheckbox";
            this.contrastCheckbox.Size = new System.Drawing.Size(134, 17);
            this.contrastCheckbox.TabIndex = 13;
            this.contrastCheckbox.Text = "Contrast Enhancement";
            this.contrastCheckbox.UseVisualStyleBackColor = true;
            // 
            // gradientCheckbox
            // 
            this.gradientCheckbox.AutoSize = true;
            this.gradientCheckbox.Location = new System.Drawing.Point(6, 71);
            this.gradientCheckbox.Name = "gradientCheckbox";
            this.gradientCheckbox.Size = new System.Drawing.Size(132, 17);
            this.gradientCheckbox.TabIndex = 12;
            this.gradientCheckbox.Text = "Color Gradient Overlay";
            this.gradientCheckbox.UseVisualStyleBackColor = true;
            // 
            // rainCheckbox
            // 
            this.rainCheckbox.AutoSize = true;
            this.rainCheckbox.Location = new System.Drawing.Point(6, 45);
            this.rainCheckbox.Name = "rainCheckbox";
            this.rainCheckbox.Size = new System.Drawing.Size(90, 17);
            this.rainCheckbox.TabIndex = 11;
            this.rainCheckbox.Text = "Rainfall Layer";
            this.rainCheckbox.UseVisualStyleBackColor = true;
            this.rainCheckbox.CheckedChanged += new System.EventHandler(this.rainCheckbox_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Rainfall export:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 412);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.aboutButton);
            this.Controls.Add(this.generateButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Dwarfpunch Mapper v 1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button biomeButton;
        private System.Windows.Forms.Label biomeLabel;
        private System.Windows.Forms.Button rainfallButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button vegButton;
        private System.Windows.Forms.Button aboutButton;
        private System.Windows.Forms.CheckBox vegCheckbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox rainCheckbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox gradientCheckbox;
        private System.Windows.Forms.CheckBox contrastCheckbox;
    }
}

