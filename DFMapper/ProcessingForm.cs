﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace DFMapper
{
    public partial class ProcessingForm : Form
    {
        internal MapProcessor map;
        internal bool veg = false;
        internal bool rain = false;
        internal bool gradient = false;
        internal bool contrast = false;
        private Form1 parentform;
        private int progress;
        private int maximum;

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        public ProcessingForm()
        {
            InitializeComponent();

            this.Shown += new EventHandler(delegate(object sender, EventArgs e)
            {
                backgroundWorker1.RunWorkerAsync();
            });
        }

        public ProcessingForm(Form1 parent)
        {
            parentform = parent;

            InitializeComponent();

            this.Shown += new EventHandler(delegate(object sender, EventArgs e)
            {
           
                //calculate weighted total value
                int initial = 5; //to indicate initial progress
                maximum = 40 + 10 + initial;  //for biome + background + initial

                if (veg)
                {
                    maximum += 20;
                }

                if (rain)
                {
                    maximum += 25;
                }

                if (gradient)
                {
                    maximum += 10;
                }

                if (contrast)
                {
                    maximum += 5;
                }

                progressBar.Value = initial;
                progressBar.Maximum = maximum;

                
                backgroundWorker1.RunWorkerAsync();
            });
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {            
            progress = progressBar.Value;
            backgroundWorker1.ReportProgress(progress, "Drawing landmass...");
            map.biome();
            progress += 40;
            backgroundWorker1.ReportProgress(progress);

            if (veg)
            {
                backgroundWorker1.ReportProgress(progress, "Drawing forests...");
                map.vegetation();
                progress += 20;
                backgroundWorker1.ReportProgress(progress);
            }

            if (rain)
            {
                backgroundWorker1.ReportProgress(progress, "Adding rainfall layer...");
                map.rainfall();
                progress += 25;
                backgroundWorker1.ReportProgress(progress);
            }

            backgroundWorker1.ReportProgress(progress, "Drawing oceans...");
            map.background();
            progress += 10;
            backgroundWorker1.ReportProgress(progress);

            if (gradient)
            {
                backgroundWorker1.ReportProgress(progress, "Applying gradient overlay...");
                map.gradientOverlay();
                progress += 10;
                backgroundWorker1.ReportProgress(progress);
            }

            if (contrast)
            {
                backgroundWorker1.ReportProgress(progress, "Enhancing contrast...");
                map.contrast();
                progress += 5;
                backgroundWorker1.ReportProgress(progress);
            }
            backgroundWorker1.ReportProgress(maximum, "Complete!");

            if (backgroundWorker1.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            //allow final message to show for a little bit
            Thread.Sleep(250);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
            progressLabel.Text = (String)e.UserState;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //close out
            if (!e.Cancelled)
            {
                Form2 resultsForm = new Form2(map, parentform);
                resultsForm.Show();
                this.Dispose();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
            parentform.Show();
            this.Dispose();
        }
    }
}
