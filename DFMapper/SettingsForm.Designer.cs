﻿namespace DFMapper
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.applyButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.scaleLabel = new System.Windows.Forms.Label();
            this.scaleSetting = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.forestOpacityLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.forestOpacity = new System.Windows.Forms.TrackBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mountainOpacityLabel = new System.Windows.Forms.Label();
            this.mountainOpacity = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.mountainColorPanel = new System.Windows.Forms.Panel();
            this.forestColorPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.mountainButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.forestColorButton = new System.Windows.Forms.Button();
            this.defaultsButton = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.mountainTintLabel = new System.Windows.Forms.Label();
            this.mountainTintSetting = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.forestTintLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.forestTintSetting = new System.Windows.Forms.TrackBar();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSetting)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.forestOpacity)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mountainOpacity)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mountainTintSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.forestTintSetting)).BeginInit();
            this.SuspendLayout();
            // 
            // applyButton
            // 
            this.applyButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.applyButton.Enabled = false;
            this.applyButton.Location = new System.Drawing.Point(352, 423);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 0;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(12, 423);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(415, 405);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.scaleLabel);
            this.tabPage1.Controls.Add(this.scaleSetting);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(407, 379);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // scaleLabel
            // 
            this.scaleLabel.AutoSize = true;
            this.scaleLabel.Location = new System.Drawing.Point(373, 8);
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(21, 13);
            this.scaleLabel.TabIndex = 3;
            this.scaleLabel.Text = "0%";
            // 
            // scaleSetting
            // 
            this.scaleSetting.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.scaleSetting.Location = new System.Drawing.Point(126, 6);
            this.scaleSetting.Maximum = 100;
            this.scaleSetting.Minimum = 1;
            this.scaleSetting.Name = "scaleSetting";
            this.scaleSetting.Size = new System.Drawing.Size(241, 45);
            this.scaleSetting.TabIndex = 2;
            this.scaleSetting.TickFrequency = 10;
            this.scaleSetting.Value = 50;
            this.scaleSetting.Scroll += new System.EventHandler(this.scaleSetting_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Image Scale (percent):";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(407, 379);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Texture Images";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.forestTintLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.forestTintSetting);
            this.groupBox1.Controls.Add(this.forestOpacityLabel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.forestOpacity);
            this.groupBox1.Location = new System.Drawing.Point(6, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 140);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Forest";
            // 
            // forestOpacityLabel
            // 
            this.forestOpacityLabel.AutoSize = true;
            this.forestOpacityLabel.Location = new System.Drawing.Point(362, 20);
            this.forestOpacityLabel.Name = "forestOpacityLabel";
            this.forestOpacityLabel.Size = new System.Drawing.Size(21, 13);
            this.forestOpacityLabel.TabIndex = 7;
            this.forestOpacityLabel.Text = "0%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Opacity:";
            // 
            // forestOpacity
            // 
            this.forestOpacity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.forestOpacity.Location = new System.Drawing.Point(72, 18);
            this.forestOpacity.Maximum = 100;
            this.forestOpacity.Name = "forestOpacity";
            this.forestOpacity.Size = new System.Drawing.Size(288, 45);
            this.forestOpacity.TabIndex = 6;
            this.forestOpacity.TickFrequency = 10;
            this.forestOpacity.Value = 50;
            this.forestOpacity.Scroll += new System.EventHandler(this.forestOpacity_Scroll);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mountainTintLabel);
            this.groupBox2.Controls.Add(this.mountainTintSetting);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.mountainOpacityLabel);
            this.groupBox2.Controls.Add(this.mountainOpacity);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 129);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mountain";
            // 
            // mountainOpacityLabel
            // 
            this.mountainOpacityLabel.AutoSize = true;
            this.mountainOpacityLabel.Location = new System.Drawing.Point(363, 18);
            this.mountainOpacityLabel.Name = "mountainOpacityLabel";
            this.mountainOpacityLabel.Size = new System.Drawing.Size(21, 13);
            this.mountainOpacityLabel.TabIndex = 5;
            this.mountainOpacityLabel.Text = "0%";
            // 
            // mountainOpacity
            // 
            this.mountainOpacity.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mountainOpacity.Location = new System.Drawing.Point(73, 16);
            this.mountainOpacity.Maximum = 100;
            this.mountainOpacity.Name = "mountainOpacity";
            this.mountainOpacity.Size = new System.Drawing.Size(288, 45);
            this.mountainOpacity.TabIndex = 4;
            this.mountainOpacity.TickFrequency = 10;
            this.mountainOpacity.Value = 50;
            this.mountainOpacity.Scroll += new System.EventHandler(this.mountainOpacity_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Opacity:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.mountainColorPanel);
            this.tabPage3.Controls.Add(this.forestColorPanel);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.mountainButton);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.forestColorButton);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(407, 379);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Colors";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // mountainColorPanel
            // 
            this.mountainColorPanel.Location = new System.Drawing.Point(305, 38);
            this.mountainColorPanel.Name = "mountainColorPanel";
            this.mountainColorPanel.Size = new System.Drawing.Size(15, 16);
            this.mountainColorPanel.TabIndex = 5;
            // 
            // forestColorPanel
            // 
            this.forestColorPanel.Location = new System.Drawing.Point(304, 10);
            this.forestColorPanel.Name = "forestColorPanel";
            this.forestColorPanel.Size = new System.Drawing.Size(16, 16);
            this.forestColorPanel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Mountain Color:";
            // 
            // mountainButton
            // 
            this.mountainButton.Location = new System.Drawing.Point(326, 35);
            this.mountainButton.Name = "mountainButton";
            this.mountainButton.Size = new System.Drawing.Size(75, 23);
            this.mountainButton.TabIndex = 2;
            this.mountainButton.Text = "Choose...";
            this.mountainButton.UseVisualStyleBackColor = true;
            this.mountainButton.Click += new System.EventHandler(this.mountainButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Forest Color:";
            // 
            // forestColorButton
            // 
            this.forestColorButton.Location = new System.Drawing.Point(326, 6);
            this.forestColorButton.Name = "forestColorButton";
            this.forestColorButton.Size = new System.Drawing.Size(75, 23);
            this.forestColorButton.TabIndex = 0;
            this.forestColorButton.Text = "Choose...";
            this.forestColorButton.UseVisualStyleBackColor = true;
            this.forestColorButton.Click += new System.EventHandler(this.forestColorButton_Click);
            // 
            // defaultsButton
            // 
            this.defaultsButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.defaultsButton.Location = new System.Drawing.Point(234, 423);
            this.defaultsButton.Name = "defaultsButton";
            this.defaultsButton.Size = new System.Drawing.Size(112, 23);
            this.defaultsButton.TabIndex = 3;
            this.defaultsButton.Text = "Restore Defaults";
            this.defaultsButton.UseVisualStyleBackColor = true;
            this.defaultsButton.Click += new System.EventHandler(this.defaultsButton_Click);
            // 
            // mountainTintLabel
            // 
            this.mountainTintLabel.AutoSize = true;
            this.mountainTintLabel.Location = new System.Drawing.Point(362, 69);
            this.mountainTintLabel.Name = "mountainTintLabel";
            this.mountainTintLabel.Size = new System.Drawing.Size(21, 13);
            this.mountainTintLabel.TabIndex = 8;
            this.mountainTintLabel.Text = "0%";
            // 
            // mountainTintSetting
            // 
            this.mountainTintSetting.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mountainTintSetting.Location = new System.Drawing.Point(72, 67);
            this.mountainTintSetting.Maximum = 100;
            this.mountainTintSetting.Name = "mountainTintSetting";
            this.mountainTintSetting.Size = new System.Drawing.Size(288, 45);
            this.mountainTintSetting.TabIndex = 7;
            this.mountainTintSetting.TickFrequency = 10;
            this.mountainTintSetting.Value = 50;
            this.mountainTintSetting.Scroll += new System.EventHandler(this.mountainTintSetting_Scroll);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tint:";
            // 
            // forestTintLabel
            // 
            this.forestTintLabel.AutoSize = true;
            this.forestTintLabel.Location = new System.Drawing.Point(362, 78);
            this.forestTintLabel.Name = "forestTintLabel";
            this.forestTintLabel.Size = new System.Drawing.Size(21, 13);
            this.forestTintLabel.TabIndex = 10;
            this.forestTintLabel.Text = "0%";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Tint:";
            // 
            // forestTintSetting
            // 
            this.forestTintSetting.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.forestTintSetting.Location = new System.Drawing.Point(72, 76);
            this.forestTintSetting.Maximum = 100;
            this.forestTintSetting.Name = "forestTintSetting";
            this.forestTintSetting.Size = new System.Drawing.Size(288, 45);
            this.forestTintSetting.TabIndex = 9;
            this.forestTintSetting.TickFrequency = 10;
            this.forestTintSetting.Value = 50;
            this.forestTintSetting.Scroll += new System.EventHandler(this.forestTintSetting_Scroll);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 458);
            this.Controls.Add(this.defaultsButton);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.applyButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scaleSetting)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.forestOpacity)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mountainOpacity)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mountainTintSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.forestTintSetting)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button forestColorButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button mountainButton;
        private System.Windows.Forms.Panel mountainColorPanel;
        private System.Windows.Forms.Panel forestColorPanel;
        private System.Windows.Forms.Button defaultsButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar scaleSetting;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.Label mountainOpacityLabel;
        private System.Windows.Forms.TrackBar mountainOpacity;
        private System.Windows.Forms.Label forestOpacityLabel;
        private System.Windows.Forms.TrackBar forestOpacity;
        private System.Windows.Forms.Label forestTintLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar forestTintSetting;
        private System.Windows.Forms.Label mountainTintLabel;
        private System.Windows.Forms.TrackBar mountainTintSetting;
        private System.Windows.Forms.Label label7;
    }
}