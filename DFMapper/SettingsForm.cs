﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DFMapper
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();

            //set defaults
            loadValues();
            applyButton.Enabled = false;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void applyButton_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.ImageScale = (float)scaleSetting.Value / 100.0f;

            Properties.Settings.Default.MountainOpactiy = (float)mountainOpacity.Value / 100.0f;
            Properties.Settings.Default.ForestOpacity = (float)forestOpacity.Value / 100.0f;
            Properties.Settings.Default.ForestTint = (float)forestTintSetting.Value / 100.0f;
            Properties.Settings.Default.MountainTint = (float)mountainTintSetting.Value / 100.0f;

            Properties.Settings.Default.ForestColor = forestColorPanel.BackColor;
            Properties.Settings.Default.MountainColor = mountainColorPanel.BackColor;

            //save setting changes
            Properties.Settings.Default.Save();
            applyButton.Enabled = false;
        }

        private void defaultsButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();
            loadValues();
            applyButton.Enabled = false;
        }

        /// <summary>
        /// Puts the correct setting values in the various components
        /// </summary>
        private void loadValues()
        {
            scaleSetting.Value = (int)(Properties.Settings.Default.ImageScale * 100);
            scaleLabel.Text = String.Format("{0}%", scaleSetting.Value);

            mountainOpacity.Value = (int)(Properties.Settings.Default.MountainOpactiy * 100);
            mountainOpacityLabel.Text = String.Format("{0}%", mountainOpacity.Value);
            forestOpacity.Value = (int)(Properties.Settings.Default.ForestOpacity * 100);
            forestOpacityLabel.Text = String.Format("{0}%", forestOpacity.Value);
            forestTintSetting.Value = (int)(Properties.Settings.Default.ForestTint * 100);
            forestTintLabel.Text = String.Format("{0}%", forestTintSetting.Value);
            mountainTintSetting.Value = (int)(Properties.Settings.Default.MountainTint * 100);
            mountainTintLabel.Text = String.Format("{0}%", mountainTintSetting.Value);

            forestColorPanel.BackColor = Properties.Settings.Default.ForestColor;
            mountainColorPanel.BackColor = Properties.Settings.Default.MountainColor;
        }

        private void forestColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                forestColorPanel.BackColor = colorDialog1.Color;
                applyButton.Enabled = true;
            }
        }

        private void mountainButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                mountainColorPanel.BackColor = colorDialog1.Color;
                applyButton.Enabled = true;
            }
        }

        private void forestOpacity_ValueChanged(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
        }

        private void scaleSetting_Scroll(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
            scaleLabel.Text = String.Format("{0}%", scaleSetting.Value);
        }

        private void mountainOpacity_Scroll(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
            mountainOpacityLabel.Text = String.Format("{0}%", mountainOpacity.Value);
        }

        private void forestOpacity_Scroll(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
            forestOpacityLabel.Text = String.Format("{0}%", forestOpacity.Value);
        }

        private void mountainTintSetting_Scroll(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
            mountainTintLabel.Text = String.Format("{0}%", mountainTintSetting.Value);
        }

        private void forestTintSetting_Scroll(object sender, EventArgs e)
        {
            applyButton.Enabled = true;
            forestTintLabel.Text = String.Format("{0}%", forestTintSetting.Value);
        }
    }
}
