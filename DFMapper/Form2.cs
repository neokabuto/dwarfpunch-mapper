﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Drawing.Imaging;

namespace DFMapper
{
    public partial class Form2 : Form
    {
        private Image result;
        private Form1 parentform;

        private Form2()
        {
            InitializeComponent();
            this.Text = String.Format("{0} - Results", AssemblyTitle);
        }

        /// <summary>
        /// Creates form with a set image on display
        /// </summary>
        /// <param name="mp">MapProcessor with Image to display</param>
        /// <param name="form1">Original form displaying this one</param>
        public Form2(MapProcessor mp, Form1 form1)
        {
            InitializeComponent();
            this.Text = String.Format("{0} - Results", AssemblyTitle);

            result = mp.Image;
            parentform = form1;

            resultPictureBox.Image = result;

            this.FormClosing += delegate(Object sender, FormClosingEventArgs e)
            {
                parentform.Show();
                mp.Dispose();
            };
        }

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }
        
        private void backButton_Click(object sender, EventArgs e)
        {
            //go back to the original form
            parentform.Show();
            this.Dispose();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.DefaultExt = ".png";

            saveFileDialog1.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|GIF Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        result.Save(saveFileDialog1.FileName, ImageFormat.Png);
                        break;
                    case 2:
                        result.Save(saveFileDialog1.FileName, ImageFormat.Bmp);
                        break;
                    case 3:
                        result.Save(saveFileDialog1.FileName, ImageFormat.Gif);
                        break;
                    case 4:
                        result.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
                        break;
                }
            }
        }
    }
}
