﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using AForge.Imaging.Textures;
using System.Threading.Tasks;

namespace DFMapper
{
    public class MapProcessor : IDisposable
    {
        /// <summary>
        /// Final output
        /// </summary>
        public Bitmap Image;

        /// <summary>
        /// Input
        /// </summary>
        public Bitmap biomeImage, rainfallImage, vegImage;

        float scale = Properties.Settings.Default.ImageScale;

        public MapProcessor(Bitmap biome)
        {
            biomeImage = biome;

            //rescale inputs
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear((int) (biome.Width * scale), (int) (biome.Height * scale)));
            biomeImage = filter.Apply(biomeImage);
        }

        public MapProcessor(Bitmap biome, Bitmap rain, Bitmap veg)
        {
            //inputs
            biomeImage = biome;
            rainfallImage = rain;
            vegImage = veg;

            //rescale inputs
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear((int) (biome.Width * scale), (int) (biome.Height * scale)));
            biomeImage = filter.Apply(biomeImage);
            rainfallImage = filter.Apply(rainfallImage);
            vegImage = filter.Apply(vegImage);

        }

        /// <summary>
        /// Remove oceans, make land grey, and then brown it all out and add mountains
        /// </summary>
        public void biome()
        {
            Image = biomeImage;

            //remove oceans
            int imgwidth = Image.Width;
            int imgheight = Image.Height;
            for (int x = 0; x < imgwidth; x++)
            {
                for (int y = 0; y < imgwidth; y++)
                {
                    Color c = Image.GetPixel(x, y);
                    if (c.GetHue() == 240)
                    {
                        Image.SetPixel(x, y, Color.White);
                    }
                    else
                    {
                        //convert to grayscale
                        Image.SetPixel(x,y, new HSL((int)c.GetHue(), 0.0f, c.GetBrightness()).ToRGB().Color);
                    }
                }
            }

            //make sepia-ish
            Color brownish = Properties.Settings.Default.MountainColor;
            Image = tintTexture(ref Image, brownish, Properties.Settings.Default.MountainTint);

            //load overlay file
            Bitmap mountains = (Bitmap)Bitmap.FromFile("mountains.jpg");

            //overlay onto image at 25%
            Image = overlayTexture(Image, mountains, Properties.Settings.Default.MountainOpactiy, 255);
        }

        /// <summary>
        /// Add vegetation
        /// </summary>
        public void vegetation()
        {
            Bitmap forest = (Bitmap)Bitmap.FromFile("forest.jpg");

            //green tint
            Color greenish = Properties.Settings.Default.ForestColor;
            vegImage = tintTexture(ref vegImage, greenish, Properties.Settings.Default.ForestTint, 0);

            //forest overlay
            vegImage = overlayTexture(vegImage, forest, Properties.Settings.Default.ForestOpacity, 0);
            Image = overlayTexture(Image, vegImage, 1.0f, 0, true);
        }

        /// <summary>
        /// Add rainfall
        /// </summary>
        public void rainfall(){
            //rainfall
            Image = overlayTexture(Image, rainfallImage, 0.15f, 255);
        }


        /// <summary>
        /// Add background
        /// </summary>
        public void background()
        {
            //create background
            Bitmap background = (Bitmap)Bitmap.FromFile("oceanfloor.png");
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear(Image.Width, Image.Height));
            background = filter.Apply(background);

            //put it all together
            for (int x = 0; x < background.Width; x++)
            {
                for (int y = 0; y < background.Height; y++)
                {
                    if (Image.GetPixel(x, y).R != 255 || Image.GetPixel(x, y).G != 255 || Image.GetPixel(x, y).B != 255) //make sure pixel is actually colored
                    {
                        background.SetPixel(x, y, Image.GetPixel(x, y));
                    }
                }
            }

            Image = background;
        }

        /// <summary>
        /// Adds gradient overlay
        /// </summary>
        public void gradientOverlay(float opacity = 0.1f){
            Bitmap overlay = (Bitmap)Bitmap.FromFile("colorgradient.png");

            float opacity2 = 1.0f - opacity;

            //scale overlay to match other sizes
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear(Image.Width, Image.Height));
            overlay = filter.Apply(overlay);

            int imgheight = Image.Height;
            int imgwidth = Image.Width;
            for (int y = 0; y < imgheight; y++)
            {
                Color c2 = overlay.GetPixel(6, y); //same over horizontal, so save CPU time by finding it only once
                Parallel.For (0, imgwidth, delegate(int x)
                {
                    Color c;
                    lock (Image) { c = Image.GetPixel(x, y); }
                    float red = opacity2 * (c.R / 255.0f) + opacity * (c2.R / 255.0f);
                    float green = opacity2 * (c.G / 255.0f) + opacity * (c2.G / 255.0f);
                    float blue = opacity2 * (c.B / 255.0f) + opacity * (c2.B / 255.0f);
                    lock (Image) { Image.SetPixel(x, y, Color.FromArgb((byte)(255.0f * red), (byte)(255.0f * green), (byte)(255.0f * blue))); }
                });
            }
        }

        /// <summary>
        /// Adjusts contrast
        /// </summary>
        public void contrast(){
            ContrastCorrection contrastFilter = new ContrastCorrection(20);
            contrastFilter.ApplyInPlace(Image);
        }

        /// <summary>
        /// Tints the bitmap by overlaying a color
        /// </summary>
        /// <param name="original">Bitmap to tint</param>
        /// <param name="color">Color to tint it</param>
        /// <param name="opacity">Opacity to tint it at</param>
        /// <param name="skip">Pixel value to skip over (255 for white, 0 for black)</param>
        /// <returns>The tinted Bitmap</returns>
        Bitmap tintTexture(ref Bitmap original, Color color, float opacity, int skip = 255)
        {
            int height = original.Height;
            int width = original.Width;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Color c = original.GetPixel(x, y);
                    if (c.R != skip || c.G != skip || c.B != skip) //make sure pixel is actually colored
                    {
                        float red = (1.0f - opacity) * (c.R / 255.0f) + opacity * (color.R / 255.0f);
                        float green = (1.0f - opacity) * (c.G / 255.0f) + opacity * (color.G / 255.0f);
                        float blue = (1.0f - opacity) * (c.B / 255.0f) + opacity * (color.B / 255.0f);

                        original.SetPixel(x, y, Color.FromArgb((byte)(255.0f * red), (byte)(255.0f * green), (byte)(255.0f * blue)));
                    }
                }
            }

            return original;
        }

        /// <summary>
        /// Overlays a texture on top of another
        /// </summary>
        /// <param name="original">texture to overlay onto</param>
        /// <param name="overlay">the overlay to add</param>
        /// <param name="opacity">opacity of the overlay</param>
        /// <param name="skip">color to "skip"</param>
        /// <param name="skipOnOverlay">if true, skipping is on overlay, not original</param>
        /// <returns></returns>
        Bitmap overlayTexture(Bitmap original, Bitmap overlay, float opacity, int skip = 0, bool skipOnOverlay = false)
        {
            int overwidth = overlay.Width;
            int overheight = overlay.Height;
            int origwidth = original.Width;
            int origheight = original.Height;
            for (int x = 0; x < origwidth; x++)
            {
                for (int y = 0; y < origheight; y++)
                {
                    Color c;
                    if (!skipOnOverlay)
                    {
                        c = original.GetPixel(x, y);
                    }
                    else
                    {
                        c = overlay.GetPixel(x, y);
                    }

                    if (c.R != skip || c.G != skip || c.B != skip) //make sure pixel is actually colored
                    {
                        if (opacity != 1.0f)
                        {
                            Color c1 = original.GetPixel(x, y);
                            Color c2 = overlay.GetPixel(x % overwidth, y % overheight);
                            float red = opacity * (c2.R / 255.0f) + (1.0f - opacity) * (c1.R / 255.0f);
                            float green = opacity * (c2.G / 255.0f) + (1.0f - opacity) * (c1.G / 255.0f);
                            float blue = opacity * (c2.B / 255.0f) + (1.0f - opacity) * (c1.B / 255.0f);

                            //overlay it
                            original.SetPixel(x, y, Color.FromArgb((byte)(255.0f * red), (byte)(255.0f * green), (byte)(255.0f * blue)));
                        }
                        else
                        {
                            original.SetPixel(x, y, overlay.GetPixel(x % overwidth, y % overheight));
                        }
                    }
                }
            }

            return original;
        }

        /// <summary>
        /// Sets rainfall layer
        /// </summary>
        /// <param name="rainfallImage">Inputted rainfall image</param>
        internal void setRainfallImage(Bitmap rainfallImage)
        {
            this.rainfallImage = rainfallImage;

            //rescale input
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear((int)(rainfallImage.Width * scale), (int)(rainfallImage.Height * scale)));
            this.rainfallImage = filter.Apply(rainfallImage);
        }

        /// <summary>
        /// Sets vegetation layer
        /// </summary>
        /// <param name="vegImage">Inputted vegetation layer image</param>
        internal void setVegImage(Bitmap vegImage)
        {
            this.vegImage = vegImage;

            //rescale input
            FiltersSequence filter = new AForge.Imaging.Filters.FiltersSequence();
            filter.Add(new AForge.Imaging.Filters.ResizeBilinear((int)(vegImage.Width * scale), (int)(vegImage.Height * scale)));
            this.vegImage = filter.Apply(vegImage);
        }

        public void Dispose()
        {
            //clean up resources
            if (Image != null)
                Image.Dispose();

            if (biomeImage != null)
                biomeImage.Dispose();

            if (rainfallImage != null)
                rainfallImage.Dispose();

            if (vegImage != null)
                vegImage.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
