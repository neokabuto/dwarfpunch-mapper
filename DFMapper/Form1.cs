﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Threading;

namespace DFMapper
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Files for processing
        /// </summary>
        string biomeFilename = "", rainfallFilename = "", vegFilename = "";


        public Form1()
        {
            InitializeComponent();
            this.Text = String.Format("{0} v {1}", AssemblyTitle, AssemblyVersion);
            openFileDialog1.Filter = "Bitmap files (*.bmp)|*.bmp";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            vegCheckbox.CheckState = CheckState.Checked;
            rainCheckbox.CheckState = CheckState.Checked;
            gradientCheckbox.CheckState = CheckState.Checked;
            contrastCheckbox.CheckState = CheckState.Checked;
        }

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + "." + Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();
            }
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            //display an about box
            AboutBox1 aboutbox = new AboutBox1();
            aboutbox.Show();
        }

        private void biomeButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select biome export image";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                biomeFilename = openFileDialog1.FileName;
                biomeLabel.ForeColor = Color.ForestGreen;
            }

            checkFiles();
        }

        private void rainfallButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select rainfall export image";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                rainfallFilename = openFileDialog1.FileName;
            }

            checkFiles();
        }

        private void vegButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select vegitation export image";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                vegFilename = openFileDialog1.FileName;
            }

            checkFiles();
        }

        /// <summary>
        /// Enables button if all required files have been chosen
        /// </summary>
        private void checkFiles()
        {
            if (biomeFilename == "")
            {
                generateButton.Enabled = false;
                return;
            }

            if (vegCheckbox.CheckState == CheckState.Checked && vegFilename == "")
            {
                generateButton.Enabled = false;
                return;
            }

            if (rainCheckbox.CheckState == CheckState.Checked && rainfallFilename == "")
            {
                generateButton.Enabled = false;
                return;
            }

            generateButton.Enabled = true;
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            ProcessingForm pform = new ProcessingForm(this);
            pform.map = new MapProcessor((Bitmap)Bitmap.FromFile(biomeFilename));

            if (rainCheckbox.CheckState == CheckState.Checked)
            {
                pform.map.setRainfallImage((Bitmap)Bitmap.FromFile(rainfallFilename));

                pform.rain = true;
            }

            if (vegCheckbox.CheckState == CheckState.Checked)
            {
                pform.map.setVegImage((Bitmap)Bitmap.FromFile(vegFilename));

                pform.veg = true;
            }

            if (gradientCheckbox.CheckState == CheckState.Checked)
            {
                pform.gradient = true;
            }

            if (contrastCheckbox.CheckState == CheckState.Checked)
            {
                pform.contrast = true;
            }

            this.Hide();
            pform.Show();
        }

        private void vegCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkFiles();
        }

        private void rainCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            checkFiles();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.Show();
        }
    }
}
